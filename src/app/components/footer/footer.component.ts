import {Component} from '@angular/core';
import {MatAnchor, MatIconAnchor} from "@angular/material/button";
import {MatIcon, MatIconModule} from "@angular/material/icon";
import {MatToolbar} from "@angular/material/toolbar";

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [
    MatAnchor,
    MatIconAnchor,
    MatIcon,
    MatIconModule,
    MatToolbar
  ],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss'
})
export class FooterComponent {

}
