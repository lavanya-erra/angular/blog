import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAnalytics, provideAnalytics, ScreenTrackingService } from '@angular/fire/analytics';
import { getPerformance, providePerformance } from '@angular/fire/performance';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), provideAnimationsAsync(), importProvidersFrom(provideFirebaseApp(() => initializeApp({"projectId":"lavanya-erra","appId":"1:521781499018:web:6814720e8e4b0ba99ac973","storageBucket":"lavanya-erra.appspot.com","apiKey":"AIzaSyBu0VV0u_SNcS5npfj7brt9vCdlLPzBHw4","authDomain":"lavanya-erra.firebaseapp.com","messagingSenderId":"521781499018","measurementId":"G-P7P3QL17HT"}))), importProvidersFrom(provideAnalytics(() => getAnalytics())), ScreenTrackingService, importProvidersFrom(providePerformance(() => getPerformance()))]
};
