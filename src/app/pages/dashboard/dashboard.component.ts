import {Component} from '@angular/core';
import {MatCardModule} from "@angular/material/card";
import {MatAnchor, MatButton} from "@angular/material/button";
import {MatChip, MatChipSet} from "@angular/material/chips";

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    MatCardModule,
    MatButton,
    MatAnchor,
    MatChipSet,
    MatChip
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {
  projects = [
    {
      name: 'Authenticator',
      description: 'A simple authentication app built using Firebase Authentication',
      stack: ['Angular', 'Firebase', 'Material', 'Tailwind'],
      image: 'authenticator.png',
      url: 'https://lerra-auth.web.app/'
    },
    {
      name: 'To Do',
      description: 'A simple TODO app built using Angular and Firebase',
      stack: ['Angular', 'Firebase', 'Material', 'Tailwind'],
      image: 'todo.png',
      url: 'https://lerra-todo.web.app/'
    },
  ]
}
